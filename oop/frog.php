<?php
    require_once('animal.php')

    class Frog extends Animal
    (
        public function jump ($nomor)
        {
            echo "hop-hop $nomor";
        }
    )